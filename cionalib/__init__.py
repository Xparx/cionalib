import os as _os
from configobj import ConfigObj
import pkg_resources
import pandas as _pd


def scanpy_assign_color(adata, obs_key='celltype', configfile='data/celltypes.cfg', config_field='colors', default_color='#ececec', as_dict=False):

    stream = pkg_resources.resource_stream(__name__, configfile)
    config = ConfigObj(stream, unrepr=True)[config_field]

    obs_cat = adata.obs[obs_key].cat.categories.tolist()
    mapped_2_list = []
    for k in obs_cat:
        if k not in config:
            mapped_2_list.append(default_color)
        else:
            mapped_2_list.append(config[k])

    if as_dict:
        mapped_2_list = {i: j for i, j in zip(obs_cat, mapped_2_list)}

    return mapped_2_list


def id_mapper(from_key='KY2019', to_key='Uniq_Name', map_file="data/feature.KH.KY.anno.tsv"):

    stream = pkg_resources.resource_stream(__name__, map_file)
    gene_map = _pd.read_csv(stream, sep='\t').set_index(from_key)[to_key]

    return gene_map


def ensembl_mapper(map_file='data/ensembl_enscing_ky_dictionary.tsv'):

    stream = pkg_resources.resource_stream(__name__, map_file)
    gene_map = _pd.read_csv(stream, sep='\t').set_index('KY2019')['ENSEMBL_GENE_ID']

    return gene_map


def moitf_mapper(from_key='MotifKYID', to_key='MotifName', map_file='data/motif_features.tsv'):

    stream = pkg_resources.resource_stream(__name__, map_file)
    motif_map = _pd.read_csv(stream, sep='\t').set_index(from_key)[to_key]

    return motif_map


def add_ghost_annotations(data, ghosttf="TFs_from_GHOST.tsv",
                          ghostsignal="signaling_molecules_from_GHOST.tsv",
                          data_var='KH2013', data_dir='data'):

    stream = pkg_resources.resource_stream(__name__, _os.path.join(data_dir, ghosttf))
    tfs = _pd.read_csv(stream, sep='\t')

    tfs['GeneID'] = tfs['GeneID'].str.replace('KH2013:', '')
    stream = pkg_resources.resource_stream(__name__, _os.path.join(data_dir, ghostsignal))
    signl = _pd.read_csv(stream, sep="\t")
    signl['GeneID'] = signl['GeneID'].str.replace('KH2013:', '')

    data.var['GHOST TF'] = [x in tfs['GeneID'].values for x in data.var[data_var]]
    data.var['GHOST Signal'] = [x in signl['GeneID'].values for x in data.var[data_var]]
    data.var['GHOST TF'] = data.var['GHOST TF'].astype(int)
    data.var['GHOST Signal'] = data.var['GHOST Signal'].astype(int)


def load_ghost_gene_annotation(tf='Transcription_Factors',
                               signal='Signaling_Molecule_genes',
                               zf='Zink_finger_genes',
                               misc='Misc_genes'):

    gene_dirs = [tf, signal, zf, misc]
    columns = ['Gene Note', 'Clone used for ISH', 'KY gene model', 'Notes']

    gene_a = []
    for gd in gene_dirs:
        dir_path = _os.path.join('data', 'Ghost_Database_HT_20220223', gd)
        out = pkg_resources.resource_listdir(__name__, dir_path)
        for f in out:
            g_class = f.split('.')[0]
            stream = pkg_resources.resource_stream(__name__, _os.path.join(dir_path, f))

            gene_ann = _pd.read_csv(stream, sep=',', header=None)
            gene_ann.columns = columns

            gene_ann['subclass'] = g_class
            gene_ann['class'] = gd
            gene_a.append(gene_ann)

    gene_a = _pd.concat(gene_a)
    gene_a['Gene Name'] = gene_a['Gene Note'].str.split(' \(', n=1, expand=True)[0]

    gene_a['Gene Name'] = gene_a['Gene Name'].str.split(' // ').str[-1].str.replace('Ci-', '')

    return gene_a


def load_GRN(path='data/Ghost_Database_HT_20220223', filename='Regulation20191224.tsv'):

    stream = pkg_resources.resource_stream(__name__, _os.path.join(path, filename))
    grn = _pd.read_csv(stream, sep='\t')

    return grn


def load_grn_with_KY(**kwargs):

    gene_class_annotation = load_ghost_gene_annotation()
    mapper = gene_class_annotation.set_index('Gene Name')['KY gene model']

    grn = load_GRN(**kwargs)
    grn['Regulator'] = grn['RegulatorName'].replace(mapper)
    grn['Target'] = grn['TargetName'].replace(mapper)

    grn['interaction'] = grn['function'].replace({'activation': 1, 'repression': -1})

    return grn


def load_wang2019_markers(filename=None):

    if filename is None:
        files = pkg_resources.resource_listdir(__name__, _os.path.join('data', 'Wang2019'))
        return files

    stream = pkg_resources.resource_stream(__name__, _os.path.join('data', 'Wang2019', filename))
    wang_file = _pd.read_csv(stream, sep='\t')

    wang_file['KH2013'] = wang_file['GeneID'].str.replace('KH2013:', '').copy()

    mapper = id_mapper(from_key='KH2013', to_key='Uniq_Name')

    wang_file['Uniq_Name'] = wang_file['KH2013'].replace(mapper)

    return wang_file
