# Data source annotation info.

## Gene annotation collected from 
- http://ghost.zool.kyoto-u.ac.jp/TF_KYHT.html,
- http://ghost.zool.kyoto-u.ac.jp/ST_KYHT.html,
- http://ghost.zool.kyoto-u.ac.jp/ZF_KYHT.html and
- http://ghost.zool.kyoto-u.ac.jp/misc_KYHT.html

### Original header for files is:
Gene Name (Bold: Genes named by the standard rule)	Clone used for ISH	KY gene model	Notes

## GRN downloaded from
- http://ghost.zool.kyoto-u.ac.jp/html1225/index.html
- http://ghost.zool.kyoto-u.ac.jp/html1225/Regulation20191224.txt


## Minor changes to the data files has been done. Unless it is annotated in the file the below should be exhaustive.

- Cers.e in the file Regulation20191224.tsv as been renamed to Cers-r.e
