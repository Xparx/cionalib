# Cionalib

Cionalib is an auxiliary library for analysing the [Ciona](https://en.wikipedia.org/wiki/Ciona) organism, specifically [Ciona Robusta](https://en.wikipedia.org/wiki/Ciona_robusta).

It contains manually and computationally curated data for the purpose of annotations.
As closely as possible all annotations use the KY Gene Model (KYoto Gene Model) Satou et al., 2019.

## Files of interest.
The [data](cionalib/data) directory contains all data for this repository.
- [feature.KH.KY.anno.tsv](cionalib/data/feature.KH.KY.anno.tsv) contains mapping information between the KY Gene Model and the KH2013 Gene model as well as the closest homolog in mouse based on BLAST search annotated in the file as "Uniq_Name".
- [Ghost_Database_HT_20220223](cionalib/data/Ghost_Database_HT_20220223) contains a manually parsed pull of data collected from the Ghost database (Satou et al., 2005) at 2022-02-23.
- [signatures_anatomy2genes_curated.tsv](cionalib/data/signatures_anatomy2genes_curated.tsv) and [signatures_lineages2anatomy.tsv](cionalib/data/signatures_lineages2anatomy.tsv) contains manually collected signatures for gene signatures and cell types from various sources.

## References
- Satou et al., 2005, Zool Sci, 22, 837-843. [DOI:10.2108/zsj.22.837](https://doi.org/10.2108/zsj.22.837)
- Satou et al., 2019, Genome Biol Evol. 11, evz228. [DOI:10.1093/gbe/evz228](https://doi.org/10.1093/gbe/evz228)
