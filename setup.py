from setuptools import setup

DISTNAME = 'Cionalib'
VERSION = '0.1'
DESCRIPTION = "Package for helping annotate Ciona."
# with open('README.rst') as f:
#     LONG_DESCRIPTION = f.read()
MAINTAINER = 'Andreas Tjarnberg'
MAINTAINER_EMAIL = 'andreas.tjarnberg@nyu.edu'
URL = 'https://gitlab.com/Xparx/cionalib'
DOWNLOAD_URL = ''
LICENSE = 'LGPL'


setup(name=DISTNAME,
      version=VERSION,
      description=DESCRIPTION,
      url=URL,
      author=MAINTAINER,
      author_email=MAINTAINER_EMAIL,
      license=LICENSE,
      packages=['cionalib'],
      python_requires='>=3.7',
      install_requires=[
          'configobj',
          'pandas'
      ],
      include_package_data=True,
      package_data={'': ['data/*.cfg', 'data/*.tsv', 'data/*.tsv.gz',
                         'data/Ghost_Database_HT/Transcription_Factors/*.csv',
                         'data/Ghost_Database_HT/Signaling_Molecule_genes/*.csv',
                         'data/Ghost_Database_HT/Zink_finger_genes/*.csv',
                         'data/Ghost_Database_HT/Misc_genes/*.csv',
                         'data/Ghost_Database_HT/Regulation20191224.tsv',
                         'data/Wang2019/*.tsv']},
      zip_safe=False)
